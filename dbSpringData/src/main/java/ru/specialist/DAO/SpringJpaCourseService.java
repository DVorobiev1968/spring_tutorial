package ru.specialist.DAO;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory; 

@Service("springJpaCourseService")
@Repository
// понижение уровня изоляции транзакции
@Transactional(isolation = Isolation.READ_COMMITTED)
public class SpringJpaCourseService implements CourseService {
	private static final Log LOG = LogFactory.getLog(SpringJpaCourseService.class);
	
	@Autowired
	private CourseRepository courseRepository;
	
	public CourseRepository getCourseRepository() {
		return courseRepository;
	}



	@Transactional(readOnly=true) 
	public List<Course> findAll() {
		return new ArrayList<Course>(courseRepository.findAll());
	}



}