package ru.specialist.dbJPA;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import ru.specialist.DAO.Course;
import ru.specialist.DAO.CourseService;

public class App 
{
    public static void main( String[] args )
    {
		ClassPathXmlApplicationContext context = 
				new ClassPathXmlApplicationContext("applicationContext.xml");
		
		CourseService courseDao = context.getBean("springJpaCourseService", CourseService.class);
		
		/*Course spring = new Course();
		spring.setTitle("Spring");
		spring.setLength(40);
		spring.setDescription("Spring framework");
		courseDao.getCourseRepository().save(spring);*/
		
		// courseDao.getCourseRepository().delete(8);
		
		
		for(Course c : courseDao.findAll())
			System.out.println(c);
		
		context.close();
    }
}
