package ru.specialist.builder;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class BuilderConfig {

	@Bean("wall")
	@Scope("prototype")
	public Brick bricks() {
		return new Brick();
	}
	
	@Bean
	public Window woodFrameWindow() {
		return new WoodFrameWindow();
	}

	@Bean
	public Window plasticFrameWindow() {
		return new PlacticFrameWindow();
	}

	@Bean
	public House house() {
		// это не просто вызов метода  woodFrameWindow()
		// если он помечен аннотацией @Bean
		House house = new House(woodFrameWindow());
		house.setHeight(2); // 2 этажа
		return house;
	}
}
