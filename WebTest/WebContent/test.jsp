<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.time.LocalTime"%>

<!-- Основные теги создания циклов, определения условий, вывода информации на страницу и т.д.  -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!--  Теги для работы с XML-документами -->
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml"%>

<!--  Теги для работы с базами данных -->
<%@ taglib prefix="s" uri="http://java.sun.com/jsp/jstl/sql"%>

<!--  Теги для форматирования и интернационализации информации (i10n и i18n) -->
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h1>
	<% 
		LocalTime now = LocalTime.now();
		out.print(String.format("Current Server time: %tR", now));
		String s = String.format("Current Server time: %tR", now);
	%>
	</h1>
	<h2>
		<%= s%>
	</h2>
	<h2>
		<c:out value="16+64*2+1000"/>
	</h2>
	<h2>
		<c:out value="${16+64*2+1000}"/>
		<c:set var="salary" value="${32000*2}" scope="request"/>
	</h2>
	<h2>
		<c:if test="${salary > 1000 }">
			<p> Salary = <c:out value="${salary}"/></p>
		</c:if>
	</h2>
    <ul>
        <c:forEach var="user" items="${users}">
            <li><c:out value="${user}" /></li>
        </c:forEach>
    </ul></body>
</html>