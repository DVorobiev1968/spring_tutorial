package ru.specialist.springdi;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App_5
{
    public static void main( String[] args )
    {
		ClassPathXmlApplicationContext context = 
				new ClassPathXmlApplicationContext("applicationContext_5.xml");

		House house=context.getBean("houseBean",House.class);
		house.buildWall();
		house.view();
		System.out.printf("House height:%d\n",house.getHeight());
		MainWindow mainWindow=context.getBean(MainWindow.class);
		mainWindow.show();
		System.out.println(house.getClass());
		/*
		* singleton
		* prototype
		* session
		* request
		* global-session */
		context.close();
    }
}
