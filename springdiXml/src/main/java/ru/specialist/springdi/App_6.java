package ru.specialist.springdi;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App_6
{
    public static void main( String[] args )
    {
		ClassPathXmlApplicationContext context = 
				new ClassPathXmlApplicationContext("applicationContext_6.xml");

		House house=context.getBean("houseBean",House.class);
		house.buildWall();
		house.installDoors();
		house.view();
		System.out.println(house);
		System.out.println(context.getBean("houseBean"));
		System.out.println(context.getBean("houseBean2"));

		MainWindow mainWindow = context.getBean(MainWindow.class);
		System.out.printf("House height:%d\n",house.getHeight());
		mainWindow.show();
		System.out.println(house.getClass());
		System.out.println(house.getDoors().get("A"));
		/*
		* singleton
		* prototype
		* session
		* request
		* global-session */
		context.close();
    }
}
