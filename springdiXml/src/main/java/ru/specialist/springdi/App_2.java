package ru.specialist.springdi;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App_2
{
    public static void main( String[] args )
    {
		ClassPathXmlApplicationContext context = 
				new ClassPathXmlApplicationContext("applicationContext_2.xml");
		// теперь context при создании дома должен вызвать конструктор с двумя параметрами,
		// увидит ранее сданный объект window и возьмет его в качестве 1-го параметра конструктора
		// со 2-м параметром просто сразу возьмет 3
		House house=context.getBean("houseBean",House.class);
		house.view();
		System.out.printf("House height:%d",house.getHeight());
		context.close();
    }
}
