package ru.specialist.springdi;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App_4
{
    public static void main( String[] args )
    {
		ClassPathXmlApplicationContext context = 
				new ClassPathXmlApplicationContext("applicationContext_4.xml");

		House house=context.getBean("houseBean",House.class);
		house.buildWall();
		house.view();
		System.out.printf("House height:%d\n",house.getHeight());
		MainWindow mainWindow=context.getBean(MainWindow.class);
		mainWindow.show();
		context.close();
    }
}
