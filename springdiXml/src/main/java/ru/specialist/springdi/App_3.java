package ru.specialist.springdi;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App_3
{
    public static void main( String[] args )
    {
		ClassPathXmlApplicationContext context = 
				new ClassPathXmlApplicationContext("applicationContext_3.xml");
		// теперь context при создании дома должен вызвать конструктор с двумя параметрами,
		// увидит ранее сданный объект window и возьмет его в качестве 1-го параметра конструктора
		// 2-й параметр был передан в applicationContext_3.xml в свойствах bean
		House house=context.getBean("houseBean",House.class);
		house.buildWall();
		house.view();
		System.out.printf("House height:%d",house.getHeight());
		context.close();
    }
}
