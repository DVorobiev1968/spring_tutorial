package ru.specialist.springdi;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App_1
{
    public static void main( String[] args )
    {
		ClassPathXmlApplicationContext context = 
				new ClassPathXmlApplicationContext("applicationContext_1.xml");
		Window window=context.getBean("windowBean",Window.class);
		new House(window).view(); // ручное DI, поскольку мы сами создали модель дома
		context.close();
    }
}
