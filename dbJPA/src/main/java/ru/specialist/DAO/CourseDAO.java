package ru.specialist.DAO;

import java.util.List;

public interface CourseDAO {
	Course findById(int id);
	List<Course> findAll();
	
	void insert(Course course);
	void update(Course course);
	List<Course> findByTitle(String title);
	void delete(int id); 
}
