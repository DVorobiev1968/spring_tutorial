package ru.specialist.springhelloxml;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.security.spec.RSAOtherPrimeInfo;


public class App 
{
    public static void main( String[] args )
    {
		ClassPathXmlApplicationContext context = 
				new ClassPathXmlApplicationContext("applicationContext.xml");
		
		PersonBean person = context.getBean("PersonBean", PersonBean.class);
		
		System.out.printf("%s - %d\n", person.getName(), person.getAge());
		person.setAge(person.getAge()+1);
		System.out.println(person);
		System.out.println(context.getBean("PersonBean"));
		System.out.println(person);
		System.out.printf("%s - %d\n", person.getName(), person.getAge());
		// в конце context закрываем
		context.close();
    }
}
