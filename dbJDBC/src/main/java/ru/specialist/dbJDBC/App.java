package ru.specialist.dbJDBC;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import ru.specialist.DAO.Course;
import ru.specialist.DAO.CourseDAO;

public class App 
{
    public static void main( String[] args )
    {
		ClassPathXmlApplicationContext context = 
				new ClassPathXmlApplicationContext("applicationContext.xml");
		CourseDAO courseDao = context.getBean(CourseDAO.class);
//
//		System.out.println(courseDao.findById(5));

//		for(Course c : courseDao.findByTitle("Обучающий курс 1"))
//			System.out.println(c);

//		Course course=new Course();
//		course.setTitle("Insert new course");
//		course.setId(100);
//		course.setDescription("Description new course 100");
//		course.setLength(100);
//		courseDao.insert(course);
//		for(Course c : courseDao.findAll())
//			System.out.println(c);

		context.close();
    }
}
